public class MajorInfo {

    private int id;
    private String name;

    public MajorInfo() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MajorInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
