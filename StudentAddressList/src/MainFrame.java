import com.oracle.deploy.update.UpdateCheck;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainFrame extends JFrame {

    public static int frameWidth = 1700;
    public static int frameHeight = 800;
    public static int tableLeftMargin = 200;

    public TablePanel tablePanel;
    public TreePanel treePanel;

    public JButton btnAddStudent;
    public JButton btnDeleteStudent;
    public JButton btnUpdateStudent;

    public StudentManager manager;

    public MainFrame() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Main Frame");
        this.pack();

        this.setResizable(false);
        this.setBounds(50, 200, frameWidth, frameHeight);
        this.setLayout(null);

        manager = new StudentManager();
        tablePanel = new TablePanel(this, manager);
        treePanel = new TreePanel(this, manager);

        tablePanel.setBounds(tableLeftMargin, 0, frameWidth - tableLeftMargin, frameHeight);
        tablePanel.setOpaque(true);
        this.add(tablePanel);

        treePanel.setBounds(0, 0, tableLeftMargin, frameHeight - 200);
        treePanel.setOpaque(true);
        this.add(treePanel);

        btnAddStudent = new JButton("Add");
        btnDeleteStudent = new JButton("Delete");
        btnUpdateStudent = new JButton("Update");

        btnAddStudent.setBounds(0, frameHeight - 200, 200, 50);
        btnUpdateStudent.setBounds(0, frameHeight - 150, 200, 50);
        btnDeleteStudent.setBounds(0, frameHeight - 100, 200, 50);

        this.add(btnAddStudent);
        this.add(btnUpdateStudent);
        this.add(btnDeleteStudent);

        btnAddStudent.addActionListener(new MyActionListener(this));
        btnUpdateStudent.addActionListener(new MyActionListener(this));
        btnDeleteStudent.addActionListener(new MyActionListener(this));

        this.setVisible(true);
    }

    class MyActionListener implements ActionListener {

        MainFrame parentFrame;

        public MyActionListener(MainFrame parentFrame) {
            this.parentFrame = parentFrame;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String text = ((JButton)(e.getSource())).getText();
            if(text.equals("Add")) {
                new FormAddFrame("Add", null, manager, parentFrame);
                return;
            }
            String id = tablePanel.getCurrentSelectedId();
            if(id.length() == 0) {
                return;
            }
            java.util.List<StudentInfo> studentInfoList = manager.getStudents();
            StudentInfo target = null;
            for(StudentInfo info: studentInfoList) {
                if(info.getId().equals(id)) {
                    target = info;
                    break;
                }
            }
            if(text.equals("Delete")) {
                manager.deleteStudent(target);
                refresh();
                return;
            }
            if(text.equals("Update")) {
                new FormAddFrame("Update", target, manager, parentFrame);
            }
        }
    }

    public void refresh() {
        tablePanel.refreshStudentInfo();
    }

    public static void main(String[] args) {
        new MainFrame();
    }
}
