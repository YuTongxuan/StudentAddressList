import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XmlExecutor implements Executor{

    public final static String studentXmlPath = ".\\students.xml";
    public final static String majorXmlPath = ".\\major.xml";

    public final static String studentRootTagName = "Students";
    public final static String studentTagName = "Student";

    public final static String majorRootTagName = "Majors";
    public final static String majorTagName = "Major";

    public List<StudentInfo> readStudentInfo() throws Exception{

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new File(studentXmlPath));

        List<StudentInfo> retStudentInfoList = new ArrayList<StudentInfo>();

        Node rootNode = doc.getElementsByTagName(studentRootTagName).item(0);
        NodeList studentNodes = rootNode.getChildNodes();

        for(int i = 0; i < studentNodes.getLength(); i++) {
            Node studentNode = studentNodes.item(i);

            if(!studentNode.hasChildNodes()) {
                continue;
            }
            NodeList propertyNodes = studentNode.getChildNodes();

            StudentInfo studentInfo = new StudentInfo();

            for(int j = 0; j < propertyNodes.getLength(); j++) {
                Node propertyNode = propertyNodes.item(j);
                String name = propertyNode.getNodeName();
                String value = propertyNode.getTextContent();

                if(!propertyNode.hasChildNodes()) {
                    continue;
                }

                if(name.equals("id")) {
                    studentInfo.setId(value);
                } else if(name.equals("name")) {
                    studentInfo.setName(value);
                } else if(name.equals("gender")) {
                    studentInfo.setGender((StudentInfo.MALE + "").equals(value) ? StudentInfo.MALE : StudentInfo.FEMALE);
                } else if(name.equals("birth")) {
                    String[] parts = value.split("-");
                    studentInfo.setYear(Integer.parseInt(parts[0]));
                    studentInfo.setMonth(Integer.parseInt(parts[1]));
                    studentInfo.setDay(Integer.parseInt(parts[2]));
                } else if(name.equals("age")) {
                    studentInfo.setAge(Integer.parseInt(value));
                } else if(name.equals("phone")) {
                    studentInfo.setPhone(value);
                } else if(name.equals("email")) {
                    studentInfo.setEmail(value);
                } else if(name.equals("majorId")) {
                    studentInfo.setMajorId(Integer.parseInt(value));
                }
            }

            retStudentInfoList.add(studentInfo);
        }

        return retStudentInfoList;
    }

    public void writeStudentInfo(List<StudentInfo> studentInfoList) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document doc = builder.newDocument();
        Element rootNode = doc.createElement(studentRootTagName);
        doc.appendChild(rootNode);
        for(StudentInfo studentInfo : studentInfoList) {
            Element node = doc.createElement(studentTagName);

            Element idNode = doc.createElement("id");
            idNode.appendChild(doc.createTextNode(studentInfo.getId()));

            Element nameNode = doc.createElement("name");
            nameNode.appendChild(doc.createTextNode(studentInfo.getName()));

            Element genderNode = doc.createElement("gender");
            genderNode.appendChild(doc.createTextNode("" + studentInfo.getGender()));

            Element ageNode = doc.createElement("age");
            ageNode.appendChild(doc.createTextNode("" + studentInfo.getAge()));

            Element birthrNode = doc.createElement("birth");
            String birthText = studentInfo.getYear() + "-" + studentInfo.getMonth() + "-"+ studentInfo.getDay();
            birthrNode.appendChild(doc.createTextNode(birthText));

            Element phoneNode = doc.createElement("phone");
            phoneNode.appendChild(doc.createTextNode(studentInfo.getPhone()));

            Element emailNode = doc.createElement("email");
            emailNode.appendChild(doc.createTextNode(studentInfo.getEmail()));

            Element majorIdNode = doc.createElement("majorId");
            majorIdNode.appendChild(doc.createTextNode("" + studentInfo.getMajorId()));

            node.appendChild(idNode);
            node.appendChild(nameNode);
            node.appendChild(genderNode);
            node.appendChild(ageNode);
            node.appendChild(birthrNode);
            node.appendChild(phoneNode);
            node.appendChild(emailNode);
            node.appendChild(majorIdNode);

            rootNode.appendChild(node);
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(new DOMSource(doc), new StreamResult(new PrintWriter(new FileOutputStream(studentXmlPath))));
    }

    public List<MajorInfo> readMajorInfo() throws Exception {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(new File(majorXmlPath));

        List<MajorInfo> retMajorInfoList = new ArrayList<MajorInfo>();

        Node rootNode = doc.getElementsByTagName(majorRootTagName).item(0);
        NodeList majorNodes = rootNode.getChildNodes();

        for(int i = 0; i < majorNodes.getLength(); i++) {
            Node majorNode = majorNodes.item(i);

            if(!majorNode.hasChildNodes()) {
                continue;
            }
            NodeList propertyNodes = majorNode.getChildNodes();

            MajorInfo majorInfo = new MajorInfo();

            for(int j = 0; j < propertyNodes.getLength(); j++) {
                Node propertyNode = propertyNodes.item(j);
                String name = propertyNode.getNodeName();
                String value = propertyNode.getTextContent();

                if(!propertyNode.hasChildNodes()) {
                    continue;
                }

                if(name.equals("id")) {
                    majorInfo.setId(Integer.parseInt(value));
                } else if(name.equals("name")) {
                    majorInfo.setName(value);
                }
            }

            retMajorInfoList.add(majorInfo);
        }

        return retMajorInfoList;
    }

    public void writeMajorInfo(List<MajorInfo> majorInfoList) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document doc = builder.newDocument();
        Element rootNode = doc.createElement(majorRootTagName);
        doc.appendChild(rootNode);
        for(MajorInfo majorInfo : majorInfoList) {
            Element node = doc.createElement(majorTagName);

            Element idNode = doc.createElement("id");
            idNode.appendChild(doc.createTextNode("" + majorInfo.getId()));

            Element nameNode = doc.createElement("name");
            nameNode.appendChild(doc.createTextNode(majorInfo.getName()));

            node.appendChild(idNode);
            node.appendChild(nameNode);

            rootNode.appendChild(node);
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(new DOMSource(doc), new StreamResult(new PrintWriter(new FileOutputStream(majorXmlPath))));
    }

    public static void writeTestData() throws Exception{

        MajorInfo major1 = new MajorInfo();
        major1.setId(0);
        major1.setName("Computer Science");

        MajorInfo major2 = new MajorInfo();
        major2.setId(1);
        major2.setName("Information Security");

        MajorInfo major3 = new MajorInfo();
        major3.setId(2);
        major3.setName("Software Engineering");

        MajorInfo major4 = new MajorInfo();
        major4.setId(3);
        major4.setName("Embedded Development");

        StudentInfo student1 = new StudentInfo();
        student1.setId("000001");
        student1.setAge(20);
        student1.setEmail("abc@163.com");
        student1.setGender(StudentInfo.FEMALE);
        student1.setName("Saber");
        student1.setYear(1992);
        student1.setMonth(8);
        student1.setDay(12);
        student1.setPhone("123456789");
        student1.setMajorId(0);

        StudentInfo student2 = new StudentInfo();
        student2.setId("000002");
        student2.setAge(21);
        student2.setEmail("eee@163.com");
        student2.setGender(StudentInfo.MALE);
        student2.setName("Lancer");
        student2.setYear(1991);
        student2.setMonth(6);
        student2.setDay(26);
        student2.setPhone("987654321");
        student2.setMajorId(0);

        StudentInfo student3 = new StudentInfo();
        student3.setId("000003");
        student3.setAge(22);
        student3.setEmail("333@163.com");
        student3.setGender(StudentInfo.MALE);
        student3.setName("Archer");
        student3.setYear(1990);
        student3.setMonth(12);
        student3.setDay(23);
        student3.setPhone("147258369");
        student3.setMajorId(1);

        StudentInfo student4 = new StudentInfo();
        student4.setId("000004");
        student4.setAge(23);
        student4.setEmail("neko@163.com");
        student4.setGender(StudentInfo.MALE);
        student4.setName("Neko");
        student4.setYear(1992);
        student4.setMonth(8);
        student4.setDay(12);
        student4.setPhone("15851808944");
        student4.setMajorId(2);

        StudentInfo student5 = new StudentInfo();
        student5.setId("000005");
        student5.setAge(13);
        student5.setEmail("lisa@gmail.com");
        student5.setGender(StudentInfo.FEMALE);
        student5.setName("Lisa");
        student5.setYear(1997);
        student5.setMonth(2);
        student5.setDay(28);
        student5.setPhone("123987");
        student5.setMajorId(2);

        List<MajorInfo> majorInfoList = Arrays.asList(new MajorInfo[]{ major1, major2, major3, major4 });
        List<StudentInfo> studentInfoList = Arrays.asList(new StudentInfo[] { student1, student2, student3, student4, student5 });

        XmlExecutor xmlExecutor = new XmlExecutor();
        xmlExecutor.writeMajorInfo(majorInfoList);
        xmlExecutor.writeStudentInfo(studentInfoList);
    }

    public static void main(String[] args) {
        try {
            writeTestData();
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
