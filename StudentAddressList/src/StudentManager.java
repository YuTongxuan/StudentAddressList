import java.io.File;
import java.util.List;

public class StudentManager {
    private Executor executor;

    private List<StudentInfo> studentInfoList;
    private List<MajorInfo> majorInfoList;

    public StudentManager() {
        executor = new XmlExecutor();
        init();
    }

    public void init() {
        File studentXmlFile = new File(XmlExecutor.studentXmlPath);
        File majorXmlFile = new File(XmlExecutor.majorXmlPath);
        if(studentXmlFile.exists() && majorXmlFile.exists()) {
            try {
                studentInfoList = executor.readStudentInfo();
                majorInfoList = executor.readMajorInfo();
            }catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else {
            try {
                studentXmlFile.createNewFile();
                majorXmlFile.createNewFile();
                XmlExecutor.writeTestData();
                studentInfoList = executor.readStudentInfo();
                majorInfoList = executor.readMajorInfo();
            }catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public int checkStudentExist(StudentInfo studentInfo) {
        for(int i = 0; i < studentInfoList.size(); i++) {
            if(studentInfoList.get(i).getId().equals(studentInfo.getId())) {
                return i;
            }
        }
        return -1;
    }

    public boolean addStudent(StudentInfo studentInfo) {
        if(checkStudentExist(studentInfo) >= 0) {
            return false;
        }
        studentInfoList.add(studentInfo);
        try {
            executor.writeStudentInfo(studentInfoList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public boolean deleteStudent(StudentInfo studentInfo) {
        int checkIndex = checkStudentExist(studentInfo);
        if(checkIndex < 0) {
            return false;
        }
        studentInfoList.remove(checkIndex);
        try {
            executor.writeStudentInfo(studentInfoList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    public boolean updateStudent(StudentInfo studentInfo) {
        int checkIndex = checkStudentExist(studentInfo);
        if(checkIndex < 0) {
            return false;
        }
        studentInfoList.set(checkIndex, studentInfo);
        try {
            executor.writeStudentInfo(studentInfoList);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    // You should not modify the list directly
    public List<StudentInfo> getStudents() {
        return studentInfoList;
    }

    // You should not modify the list directly
    public List<MajorInfo> getMajors() {
        return majorInfoList;
    }

    public String getMajorName(int majorId) {
        for(MajorInfo majorInfo : majorInfoList) {
            if(majorInfo.getId() == majorId) {
                return majorInfo.getName();
            }
        }
        return "";
    }

    public int getMajorId(String majorName) {
        for(MajorInfo majorInfo : majorInfoList) {
            if(majorInfo.getName().equals(majorName)) {
                return majorInfo.getId();
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        StudentManager manager = new StudentManager();

        System.out.println(manager.getStudents());
    }
}
