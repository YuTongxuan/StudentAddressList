import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.UIManager;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;

import java.awt.*;
import java.net.URL;
import java.io.IOException;

public class TreePanel extends JPanel implements TreeSelectionListener {
    private JTree tree;
    private static boolean playWithLineStyle = false;
    private static String lineStyle = "Horizontal";
    private static boolean useSystemLookAndFeel = false;

    private MainFrame parentFrame = null;

    private StudentManager manager;

    public TreePanel(MainFrame parentFrame, StudentManager manager) {
        super(new GridLayout(1,0));
        DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("All majors");

        this.manager = manager;
        createNodes(rootNode);

        this.parentFrame = parentFrame;


        tree = new JTree(rootNode);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

        tree.addTreeSelectionListener(this);

        if (playWithLineStyle) {
            System.out.println("line style = " + lineStyle);
            tree.putClientProperty("JTree.lineStyle", lineStyle);
        }

        JScrollPane treeView = new JScrollPane(tree);

        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        splitPane.setTopComponent(treeView);

        Dimension minimumSize = new Dimension(100, 50);
        treeView.setMinimumSize(minimumSize);
        splitPane.setDividerLocation(100);
        splitPane.setPreferredSize(new Dimension(500, 300));

        add(splitPane);

        tree.setFont(new Font("Dialog", Font.PLAIN, 15));
    }

    // Required by TreeSelectionListener interface.
    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

        if (node != null) {
            parentFrame.tablePanel.refreshStudentInfo(node.getUserObject().toString());
        }
    }

    public void createNodes(DefaultMutableTreeNode rootNode) {
        java.util.List<MajorInfo> majorInfoList = manager.getMajors();
        for(MajorInfo info : majorInfoList) {
            rootNode.add(new DefaultMutableTreeNode(info.getName()));
        }
    }
}