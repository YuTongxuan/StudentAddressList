import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.*;
import java.awt.*;
import java.util.ArrayList;

public class TablePanel extends JPanel {

    private MainFrame parentFrame = null;
    private String currentMajorName = "";

    private JTable table;
    private StudentManager manager;


    public TablePanel(MainFrame parentFrame, StudentManager manager) {
        super(new GridLayout(1,0));
        this.parentFrame = parentFrame;
        this.manager = manager;
        table = new JTable(getTableModel(),getTableColumnModel()) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        table.getTableHeader().setFont(new Font("Dialog", Font.BOLD, 20));
        table.setFont(new Font("Dialog", Font.PLAIN, 20));
        table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setRowHeight(30);
        table.setFillsViewportHeight(true);
        JScrollPane scrollPane = new JScrollPane(table);
        this.add(scrollPane);

        refreshStudentInfo("");
    }

    public TableModel getTableModel() {
        return new AbstractTableModel() {
            public int getColumnCount() {
                return 8;
            }
            public int getRowCount() {
                java.util.List<StudentInfo> filteredList = new ArrayList<>();
                for (StudentInfo info : manager.getStudents()) {
                    if(currentMajorName.length() == 0 || currentMajorName.equals(manager.getMajorName(info.getMajorId()))) {
                        filteredList.add(info);
                    }
                }
                return filteredList.size();
            }

            public Object getValueAt(int row, int col) {
                java.util.List<StudentInfo> filteredList = new ArrayList<>();
                for (StudentInfo info : manager.getStudents()) {
                    if(currentMajorName.length() == 0 || currentMajorName.equals(manager.getMajorName(info.getMajorId()))) {
                        filteredList.add(info);
                    }
                }
                StudentInfo info = filteredList.get(row);
                String[] targetArray = new String[] {
                        info.getId(),
                        info.getName(),
                        info.getGender() == StudentInfo.MALE ? "Male" : "Female",
                        info.getAge() + "",
                        info.getYear() + "-" + info.getMonth() + "-" + info.getDay(),
                        info.getPhone(),
                        info.getEmail(),
                        manager.getMajorName(info.getMajorId())
                };
                return targetArray[col];
            }
        };
    }

    public TableColumnModel getTableColumnModel() {
        String[] columnNames = {
                "ID",
                "Name",
                "Gender",
                "Age",
                "Birthday",
                "Phone",
                "Email",
                "Major"
        };
        DefaultTableColumnModel retModel = new DefaultTableColumnModel();
        int cnt = 0;
        for(String columnName : columnNames) {
            TableColumn c = new TableColumn(cnt);
            c.setHeaderValue(columnName);
            retModel.addColumn(c);
            cnt++;
        }
        return retModel;
    }

    public void refreshStudentInfo(String majorName) {
        this.currentMajorName = majorName.equals("All majors") ? "" : majorName;
        table.updateUI();
    }

    public void refreshStudentInfo() {
        refreshStudentInfo(currentMajorName);
    }

    public String getCurrentSelectedId() {
        try {
            return table.getValueAt(table.getSelectedRow(),0).toString();
        }
        catch (Exception ex) {
            return "";
        }
    }
}