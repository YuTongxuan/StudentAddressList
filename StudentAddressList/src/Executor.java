import java.util.List;

public interface Executor {

    public List<StudentInfo> readStudentInfo() throws Exception;

    public void writeStudentInfo(List<StudentInfo> studentInfoList) throws Exception;

    public List<MajorInfo> readMajorInfo() throws Exception;

    public void writeMajorInfo(List<MajorInfo> majorInfoList) throws Exception;
}
