import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FormAddFrame {
    private Frame frame;
    private Button btnAdd;
    private Button btnClose;

    private TextField tfNum;
    private TextField tfName;
    private TextField tfGender;
    private TextField tfBirth;
    private TextField tfAge;
    private TextField tfPhone;
    private TextField tfEmail;
    private TextField tfAddress;
    private TextField tfMajor;

    private Label labelNum;
    private Label labelGender;
    private Label labelBirth;
    private Label labelName;
    private Label labelAge;
    private Label labelPhone;
    private Label labelEmail;
    private Label labelAddress;
    private Label labelMajor;

    private int buttonXOffset = 0;
    private int buttonYOffset = 0;

    private StudentManager manager;
    private String command;
    private StudentInfo info;
    private MainFrame parentFrame;

    public FormAddFrame(String command, StudentInfo studentInfo, StudentManager manager, MainFrame parentFrame) {

        this.command = command;
        this.manager = manager;
        this.info = studentInfo;
        this.parentFrame = parentFrame;

        init();
        setBounds();
        addComps();
        addListeners();
    }

    private void init() {
        frame = new Frame("Form Add");
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setLayout(null);
        frame.setFocusableWindowState(true);

        tfNum = new TextField(info == null ? "" : info.getId());
        tfName = new TextField(info == null ? "" : info.getName());
        tfAge = new TextField(info == null ? "" : info.getAge() + "");
        tfGender = new TextField(info == null ? "" : (info.getGender() == StudentInfo.MALE ? "Male" : "Female"));
        tfBirth = new TextField(info == null ? "" : info.getYear() + "-" + info.getMonth() + "-" + info.getDay());
        tfPhone = new TextField(info == null ? "" : info.getPhone());
        tfEmail = new TextField(info == null ? "" : info.getEmail());
        tfAddress = new TextField(info == null ? "" : "");
        tfMajor = new TextField(info == null ? "" : manager.getMajorName(info.getMajorId()));

        labelNum = new Label("ID");
        labelName = new Label("Name");
        labelGender = new Label("Gender");
        labelAge = new Label("Age");
        labelBirth = new Label("Birthday");
        labelPhone = new Label("Phone");
        labelEmail = new Label("Email");
        labelAddress = new Label("Address");
        labelMajor = new Label("Major");

        btnAdd = new Button(command);
        btnClose = new Button("Close");
    }

    private void setBounds() {
        frame.setBounds(600, 200, 600, 600);

        btnAdd.setBounds(buttonXOffset + 10, buttonYOffset + 40, 50, 50);
        btnClose.setBounds(buttonXOffset + 70, buttonYOffset + 40, 50, 50);

        tfNum.setBounds(buttonXOffset + 240, buttonYOffset + 150, 200, 30);
        tfName.setBounds(buttonXOffset + 240, buttonYOffset + 190, 200, 30);
        tfGender.setBounds(buttonXOffset + 240, buttonYOffset + 230, 200, 30);
        tfAge.setBounds(buttonXOffset + 240, buttonYOffset + 270, 200, 30);
        tfBirth.setBounds(buttonXOffset + 240, buttonYOffset + 310, 200, 30);
        tfPhone.setBounds(buttonXOffset + 240, buttonYOffset + 350, 200, 30);
        tfEmail.setBounds(buttonXOffset + 240, buttonYOffset + 390, 200, 30);
//      tfAddress.setBounds(buttonXOffset + 240, buttonYOffset + 430, 200, 30);
        tfMajor.setBounds(buttonXOffset + 240, buttonYOffset + 430, 200, 30);

        labelNum.setBounds(buttonXOffset + 150, buttonYOffset + 150, 100, 30);
        labelName.setBounds(buttonXOffset + 150, buttonYOffset + 190, 100, 30);
        labelGender.setBounds(buttonXOffset + 150, buttonYOffset + 230, 100, 30);
        labelAge.setBounds(buttonXOffset + 150, buttonYOffset + 270, 100, 30);
        labelBirth.setBounds(buttonXOffset + 150, buttonYOffset + 310, 100, 30);
        labelPhone.setBounds(buttonXOffset + 150, buttonYOffset + 350, 100, 30);
        labelEmail.setBounds(buttonXOffset + 150, buttonYOffset + 390, 100, 30);
//      labelAddress.setBounds(buttonXOffset + 150, buttonYOffset + 430, 100, 30);
        labelMajor.setBounds(buttonXOffset + 150, buttonYOffset + 430, 100, 30);
    }

    private void addComps() {
        frame.add(btnAdd);
        frame.add(btnClose);

        frame.add(tfNum);
        frame.add(tfName);
        frame.add(tfAge);
        frame.add(tfPhone);
        frame.add(tfEmail);
//      frame.add(tfAddress);
        frame.add(tfMajor);
        frame.add(tfGender);
        frame.add(tfBirth);

        frame.add(labelNum);
        frame.add(labelGender);
        frame.add(labelBirth);
        frame.add(labelName);
        frame.add(labelAge);
        frame.add(labelPhone);
        frame.add(labelEmail);
//      frame.add(labelAddress);
        frame.add(labelMajor);
    }

    private void addListeners() {
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();
            }
        });

        btnClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });

        btnAdd.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!tfGender.getText().equals("Male") && !tfGender.getText().equals("Female")) {
                    System.out.println("Bad Gender");
                    return;
                }
                if(manager.getMajorId(tfMajor.getText()) < 0) {
                    System.out.println("Bad Major");
                    return;
                }
                StudentInfo newInfo = new StudentInfo();
                newInfo.setId(tfNum.getText());
                newInfo.setName(tfName.getText());
                newInfo.setGender((StudentInfo.MALE + "").equals(tfGender.getText()) ? StudentInfo.MALE : StudentInfo.FEMALE);
                String[] parts = tfBirth.getText().split("-");
                newInfo.setYear(Integer.parseInt(parts[0]));
                newInfo.setMonth(Integer.parseInt(parts[1]));
                newInfo.setDay(Integer.parseInt(parts[2]));
                newInfo.setAge(Integer.parseInt(tfAge.getText()));
                newInfo.setPhone(tfPhone.getText());
                newInfo.setEmail(tfEmail.getText());

                if(command.equals("Add")) {
                    manager.addStudent(newInfo);
                }
                else if(command.equals("Update")) {
                    manager.updateStudent(newInfo);
                }
                else {
                    System.out.println("Bad command");
                }
                FormAddFrame.this.parentFrame.refresh();

                FormAddFrame.this.dispose();
            }
        });
    }

    private void dispose() {
        frame.dispose();
    }
}
