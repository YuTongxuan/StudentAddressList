import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CalculatorButtonAction implements ActionListener {

    private boolean isInputting = true;//正在输入当前数字，不是指所有都输入完了
    private TextField calculatorTextField = null;
    private String registeredFlag = "";//用来存储运算符
    double lastNumber = 0;   //可能包含小数点运算，用double型

    public CalculatorButtonAction(TextField textField) {
        this.calculatorTextField = textField;
    }

    @Override
    public void actionPerformed(ActionEvent e) {//实际操作
        Button sourceButton = (Button) e.getSource();//得到触发源上的按钮
        String text = sourceButton.getLabel();//得到触发的button上的文字
        if(text.equals("AC")) {
            isInputting = true;
            calculatorTextField.setText("0");
            registeredFlag = "";
        } else if(text.equals("DEL")) {
            if(isInputting) {
                String fieldText = calculatorTextField.getText();
                calculatorTextField.setText(fieldText.substring(0, fieldText.length() - 1)); //文本框里截取前n-1位
                if(calculatorTextField.getText().length() == 0) {
                    calculatorTextField.setText("0");//如果删去一位后删完了，则文本框置0
                }
            }
        } else if(text.equals("=")) {
            if(!registeredFlag.equals("")) { //表示之前输入的运算符和数据配套，避免只输了运算符没输后面数据的情况
                calculatorTextField.setText(calculate());
                registeredFlag = "";
                isInputting = false;
            }
        } else if("+-*/".contains(text) || text.equals("MOD") || text.equals("X^Y")) { //双目运算符运算
            isInputting = false;
            if (!registeredFlag.equals("")) { //支持连续运算
                calculatorTextField.setText(calculate());
            }
            registeredFlag = text;
        } else if(text.equals("1/X") || text.equals("SQRT")) {    //单目运算符运算
            registeredFlag = text;
            calculateSingleFlag();
        } else if(text.equals(".")) {      //若包含小数点运算
            if(!calculatorTextField.getText().contains(".")) {
                calculatorTextField.setText(calculatorTextField.getText() + ".");
            }
        } else if("0123456789".contains(text)) {
            if(isInputting) {//如果还没输完
                if(calculatorTextField.getText().equals("0")) {
                    calculatorTextField.setText("");
                }
                calculatorTextField.setText(calculatorTextField.getText() + text);
            } else {
                lastNumber = Double.parseDouble(calculatorTextField.getText()); //将文本框字符串转化成double型
                calculatorTextField.setText(text);
                isInputting = true;
            }
        }
//      System.out.println(text);
    }

    public String calculate() { //(双目）运算符
        double number = lastNumber;
        if(registeredFlag.equals("+")) {
            number += Double.parseDouble(calculatorTextField.getText());
        } else if(registeredFlag.equals("-")) {
            number -= Double.parseDouble(calculatorTextField.getText());
        } else if(registeredFlag.equals("*")) {
            number *= Double.parseDouble(calculatorTextField.getText());
        } else if(registeredFlag.equals("/")) {
            number /= Double.parseDouble(calculatorTextField.getText());
        } else if(registeredFlag.equals("MOD")) {
            number = ((int) number) % (int) Double.parseDouble(calculatorTextField.getText());
        } else if(registeredFlag.equals("X^Y")) {
            number = Math.pow(number, Double.parseDouble(calculatorTextField.getText()));
        }
        String result = "" + number;
        if(result.endsWith(".0")) {
            result = result.substring(0, result.length() - 2); //若最后运算正好为整数，则把.0去掉
        }
        return result;
    }

    public void calculateSingleFlag() {//单目运算符
        double number = Double.parseDouble(calculatorTextField.getText());
        if(registeredFlag.equals("1/X")) {
            number = 1 / number;
        } else if(registeredFlag.equals("SQRT")) {
            number = Math.sqrt(number);
        }
        String result = "" + number;
        if(result.endsWith(".0")) {
            result = result.substring(0, result.length() - 2);
        }
        registeredFlag = "";
        calculatorTextField.setText(result);
    }
}
