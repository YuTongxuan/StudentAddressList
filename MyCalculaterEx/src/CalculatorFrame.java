import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CalculatorFrame {
    private Frame frame;//窗体
    private TextField textField; //文本框

    private Button acButton;
    private Button delButton;
    private Button calcButton;
    private Button docButton;
    private Button[] numberButtons;
    private Button[] flagButtons; //按钮

    //private Button otherCaculatorButton;

    private int buttonXOffset = -10;
    private int buttonYOffset = -30;   //定位按钮整体在窗体中的位置，方便固定下按钮的相对位置后整体调整

    public CalculatorFrame() {
        init(); //初始化，设置标题、是否可见、是否可调大小、布局、焦点等
        setBounds();  //设定各控件的位置和尺寸大小
        addComps();//添加控件
        addListeners(); //添加监听器
    }

    private void init() {
        frame = new Frame("My Calculator");//窗体名称
        frame.setVisible(true); //系统初始的是窗体不可见，调为可见
        frame.setResizable(false);//设置是否可以调整窗体大小
        frame.setLayout(null);//设置布局，null布局为绝对布局，即通过手动指定位置
        frame.setFocusableWindowState(true);//设置焦点（按钮周围一圈虚线或光标），当前窗体一定有一个按钮处于激活状态

        textField = new TextField("0");//文本框初始文字设为0
        textField.setEditable(false);

        acButton = new Button("AC");
        delButton = new Button("DEL");
        calcButton = new Button("=");
        docButton = new Button(".");
        numberButtons = new Button[10];
        for (int i = 0; i < 10; i++) {
            numberButtons[i] = new Button("" + i);
        }
        flagButtons = new Button[8];
        flagButtons[0] = new Button("+");
        flagButtons[1] = new Button("-");
        flagButtons[2] = new Button("*");
        flagButtons[3] = new Button("/");
        flagButtons[4] = new Button("MOD");
        flagButtons[5] = new Button("SQRT");
        flagButtons[6] = new Button("1/X");
        flagButtons[7] = new Button("X^Y");

        //otherCaculatorButton = new Button("Loan Calculator");
    }

    private void setBounds() {
        frame.setBounds(600, 200, 610, 630);
        textField.setBounds(10, 60, 590, 40);

        acButton.setBounds(buttonXOffset + 50, buttonYOffset + 180, 210, 70);
        delButton.setBounds(buttonXOffset + 270, buttonYOffset + 180, 210, 70);

        numberButtons[7].setBounds(buttonXOffset + 50, buttonYOffset + 260, 100, 70);
        numberButtons[8].setBounds(buttonXOffset + 160, buttonYOffset + 260, 100, 70);
        numberButtons[9].setBounds(buttonXOffset + 270, buttonYOffset + 260, 100, 70);

        numberButtons[4].setBounds(buttonXOffset + 50, buttonYOffset + 340, 100, 70);
        numberButtons[5].setBounds(buttonXOffset + 160, buttonYOffset + 340, 100, 70);
        numberButtons[6].setBounds(buttonXOffset + 270, buttonYOffset + 340, 100, 70);

        numberButtons[1].setBounds(buttonXOffset + 50, buttonYOffset + 420, 100, 70);
        numberButtons[2].setBounds(buttonXOffset + 160, buttonYOffset + 420, 100, 70);
        numberButtons[3].setBounds(buttonXOffset + 270, buttonYOffset + 420, 100, 70);

        numberButtons[0].setBounds(buttonXOffset + 50, buttonYOffset + 500, 210, 70);
        calcButton.setBounds(buttonXOffset + 270, buttonYOffset + 500, 100, 70);
        docButton.setBounds(buttonXOffset + 490, buttonYOffset + 500, 100, 70);

        flagButtons[3].setBounds(buttonXOffset + 380, buttonYOffset + 260, 100, 70);
        flagButtons[2].setBounds(buttonXOffset + 380, buttonYOffset + 340, 100, 70);
        flagButtons[1].setBounds(buttonXOffset + 380, buttonYOffset + 420, 100, 70);
        flagButtons[0].setBounds(buttonXOffset + 380, buttonYOffset + 500, 100, 70);

        flagButtons[4].setBounds(buttonXOffset + 490, buttonYOffset + 180, 100, 70);
        flagButtons[5].setBounds(buttonXOffset + 490, buttonYOffset + 260, 100, 70);
        flagButtons[6].setBounds(buttonXOffset + 490, buttonYOffset + 340, 100, 70);
        flagButtons[7].setBounds(buttonXOffset + 490, buttonYOffset + 420, 100, 70);

        //otherCaculatorButton.setBounds(buttonXOffset + 420, buttonYOffset + 580, 150, 50);
    }//button.setBounds共四个参数，x,y,width,height

    private void addComps() {
        frame.add(textField);
        frame.add(acButton);
        frame.add(delButton);
        frame.add(calcButton);
        frame.add(docButton);
        for (Button btn : numberButtons) { //for-each 循环
            frame.add(btn);
        }
        for (Button btn : flagButtons) {
            frame.add(btn);
        }

      //  frame.add(otherCaculatorButton);
    }//添加控件

    private void addListeners() {//监听器
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.dispose();  //销毁程序中指定的图形界面资源，对数据资源不产生影响
            }
        });
        /*otherCaculatorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new CalculatorFrame();
            }
        });//给窗口加监听器，触发windows关闭（system.exit),默认不会自动退出，必须点×才能关掉*/
        CalculatorButtonAction action = new CalculatorButtonAction(textField);//传入的是ui里的textfield
        acButton.addActionListener(action);
        delButton.addActionListener(action);
        calcButton.addActionListener(action);
        docButton.addActionListener(action);
        for (Button btn : numberButtons) {
            btn.addActionListener(action);
        }
        for (Button btn : flagButtons) {
            btn.addActionListener(action);
        }
    }

    public static void main(String[] args) {
        CalculatorFrame frame = new CalculatorFrame();
    }
}
